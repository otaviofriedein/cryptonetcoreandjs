﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            string json = "{'nome':'otavio'}";

            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            var publicKey = rsa.ToXmlString(false); // false to get the public key   
            //string publicKey = "ChavePublica";
            var privateKey = rsa.ToXmlString(true); // true to get the private key   

            // Convert the text to an array of bytes   
            UnicodeEncoding byteConverter = new UnicodeEncoding();
            byte[] dataToEncrypt = byteConverter.GetBytes(json);

            // Create a byte array to store the encrypted data in it   
            byte[] encryptedData;
            using (RSACryptoServiceProvider newRSA = new RSACryptoServiceProvider())
            {
                // Set the rsa pulic key   
                newRSA.FromXmlString(publicKey);

                // Encrypt the data and store it in the encyptedData Array   
                encryptedData = newRSA.Encrypt(dataToEncrypt, false);
            }

            ViewBag.JSON = encryptedData;
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
